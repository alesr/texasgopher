FROM golang:1.10-stretch

LABEL maintainer="Alessandro Resta <alessandro.resta@gmail.com>"

WORKDIR $GOPATH/src/gitlab.com/alesr/texasgopher

COPY ./cmd/ cmd/

COPY ./internal/ internal/

COPY ./vendor/ vendor/

COPY ./docker-compose.yml .docker-compose.yml

RUN go build -race -o texasgopherserver cmd/server/main.go

ENV \
  PORT="8080" \
  ENVIRONMENT="dev" \
  LOG_LEVEL="info"

EXPOSE 8080
