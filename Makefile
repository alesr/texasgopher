PROJECT_NAME := TEXASGOPHER

.PHONY: help
help:
	@echo "------------------------------------------------------------------------"
	@echo "${PROJECT_NAME}"
	@echo "------------------------------------------------------------------------"
	@grep -E '^[a-zA-Z0-9_/%\-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

dep: ## download dependencies
	@go get -u github.com/golang/dep/cmd/dep
	@go get -u github.com/alecthomas/gometalinter
	@gometalinter --install
	@dep ensure

build: dep ## build application binary
	@go build -race -o texasgopherserver cmd/server/main.go

.PHONY: fmt
fmt: ## run gofmt
	@gofmt -e -l -s \
	./internal \
	./cmd/server

.PHONY: lint
lint: ## run linter
	@gometalinter \
		--tests \
		--vendor \
		--disable=gosec \
		--disable=megacheck \
		--exclude=vendor \
		./...

.PHONY: test
test: fmt ## run unit tests
	@GOCACHE=off go test -v -race -cover \
	./internal/...

.PHONY: test-unit
test-unit: ## run gofmt, linter and unit tests in docker container
	@docker-compose \
		-f docker-compose.yml \
		up --build --abort-on-container-exit unit-driver

.PHONY: test-it
test-it: ## run integration tests using docker containers
	@docker-compose \
		-f docker-compose.yml \
		up --build --abort-on-container-exit it-driver

.PHONY: up
up: ## run server
	@docker-compose \
		-f docker-compose.yml \
		up --build --abort-on-container-exit texasgopherserver

.PHONY: down
down: ## stop server
	@docker-compose -f docker-compose.yml down -v
