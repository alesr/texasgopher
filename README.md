# README

## Texas Gopher

Texas Gopher implements a web service serving a resource `cards` with a single HTTP endpoint `POST /evaluate` for evaluating the best possible poker hand for a given input.

### HowTo

1. Start Docker container `make up`

2. Request best hand evaluation: `curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"KS AH 2H 3C 4H KC 2C TC 2D AS"}'`

      ```
      HTTP/1.1 200 OK
      Content-Type: application/json
      Date: Mon, 06 Aug 2018 10:48:03 GMT
      Content-Length: 75

      {"hand":"KS AH 2H 3C 4H","deck":"KC 2C TC 2D AS","best_hand":"full-house"}
      ```

### Sample Input

#### Highest card

```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"3D 5S 2H QD TD 6S KH 9H AD 7C"}'
````

#### One pair
```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"3D 5S 2H QD TD 6S KH 9H AD QH"}'
````

#### Two pairs
```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"AH 2C 9S AD 3C QH KS JS JD KD"}' 
````

#### Three of a kind
```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"KS AH 2H KH 4H KC 3C TC 7D QS"}' 
````

#### Straight
```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"AC 2D 9C 3S KD 5S 4D KS AS 4C"}' 
````

#### Flush
```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"2H AD 5H AC 7H AH 6H 9H 4H 3C"}'
````

#### Full house
```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"KS AH 2H 3C 4H KC 2C TC 2D AS"}' 
````

#### Four of a kind
```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"2H 2S 3H 3S 3C 2D 9C 3D 6C TH"}' 
````

#### Straight flush
```
curl -i -X "POST" -H "Content-Type: application/json" http://localhost:8080/cards/evaluate -d '{"cards":"TH JH 2S QD KH QH QS AH QC 6S"}' 
````

### Benchmark

#### Best hand

| FUNC                     |       RUNS |      SPEED |
| ------------------------ | ---------- | ---------- |
| BenchmarkHighCard-8      | 2000000000 | 0.31 ns/op |
| BenchmarkOnePair-8       |    5000000 |  301 ns/op |
| BenchmarkTwoPairs-8      |    3000000 |  486 ns/op |
| BenchmarkThreeOfaKind-8  |    3000000 |  436 ns/op |
| BenchmarkStraight-8      |     500000 | 2914 ns/op |
| BenchmarkFlush-8         |    5000000 |  256 ns/op |
| BenchmarkFullHouse-8     |    2000000 |  735 ns/op |
| BenchmarkFourOfAKind-8   |    5000000 |  322 ns/op |
| BenchmarkStraightFlush-8 |     500000 | 2374 ns/op |

#### Server


Method: `POST`
Cards: `KS AH 2H 3C 4H KC 2C TC 2D AS`

```
==========================BENCHMARK==========================
URL:                          http://localhost:8080/cards/evaluate

Used Connections:             100
Used Threads:                 1
Total number of calls:        1000

===========================TIMINGS===========================
Total time passed:            1.94s
Avg time per request:         175.45ms
Requests per second:          515.82
Median time per request:      176.84ms
99th percentile time:         232.30ms
Slowest time for request:     295.00ms

=============================DATA=============================
Total response body sizes:          75000
Avg response body per request:      75.00 Byte
Transfer rate per second:           38686.14 Byte/s (0.04 MByte/s)
==========================RESPONSES==========================
20X Responses:          1000  (100.00%)
30X Responses:          0     (0.00%)
40X Responses:          0     (0.00%)
50X Responses:          0     (0.00%)
Errors:                 0     (0.00%)
```

NOTE: 

All benchmark results are merely illustrative use cases. Runs were made in the development machine with no requirements for proper testing.

### Other

```
------------------------------------------------------------------------
TEXASGOPHER
------------------------------------------------------------------------
build                          build application binary
dep                            download dependencies
down                           stop server
fmt                            run gofmt
lint                           run linter
test-it                        run integration tests using docker containers
test-unit                      run gofmt, linter and unit tests in docker container
test                           run unit tests
up                             run server
```
