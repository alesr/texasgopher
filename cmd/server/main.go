package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"

	"gitlab.com/alesr/texasgopher/internal/config"
	"gitlab.com/alesr/texasgopher/internal/service"
	"go.uber.org/zap"
)

func main() {
	conf, err := config.New()
	if err != nil {
		log.Fatalln("could not instantiate new configuration:", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	service, err := service.New(ctx, conf)
	if err != nil {
		conf.Logger.Fatal("could not instantiate new service", zap.Error(err))
	}

	conf.Logger.Info("initializing service...")

	go func() {
		if err := service.Start(); err != nil {
			if err != http.ErrServerClosed {
				conf.Logger.Fatal("could not start service", zap.Error(err))
			}
			conf.Logger.Info("server closed")
		}
	}()

	signalCh := make(chan os.Signal, 1)
	doneCh := make(chan struct{})

	signal.Notify(signalCh, os.Interrupt)

	go func() {
		<-signalCh
		log.Println("received signal to interrupt, stopping services...")
		if err := service.Stop(ctx, cancel); err != nil {
			log.Fatalln("could not stop the service:", err)
		}
		close(doneCh)
	}()
	<-doneCh
}
