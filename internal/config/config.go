package config

import (
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Enumerate server default timeouts.
const (
	defaultServerReadTimeout  = 5 * time.Second
	defaultServerWriteTimeout = 10 * time.Second
	defaultServerIdleTimeout  = 15 * time.Second
)

// Config holds configuration values for the application's services.
type Config struct {
	Addr                      string
	DefaultServerReadTimeout  time.Duration
	DefaultServerWriteTimeout time.Duration
	DefaultServerIdleTimeout  time.Duration
	Logger                    *zap.Logger
	StdLogger                 *log.Logger
}

// New instantiate a new instance of the configuration.
func New() (*Config, error) {
	port := os.Getenv("PORT")
	if port == "" {
		panic("missing server port")
	}

	env := os.Getenv("ENVIRONMENT")

	logLevel := os.Getenv("LOG_LEVEL")
	if logLevel == "" {
		panic("missing log level")
	}

	logger, stdLogger, err := newLogger(env, logLevel)
	if err != nil {
		return nil, fmt.Errorf("could not instantiate new logger: %s", err)
	}

	return &Config{
		Addr: net.JoinHostPort("", port),
		DefaultServerReadTimeout:  defaultServerReadTimeout,
		DefaultServerWriteTimeout: defaultServerWriteTimeout,
		DefaultServerIdleTimeout:  defaultServerIdleTimeout,
		Logger:    logger,
		StdLogger: stdLogger,
	}, nil
}

func newLogger(env, level string) (*zap.Logger, *log.Logger, error) {
	loggerConfig := zap.NewProductionConfig()
	if env != "production" {
		loggerConfig = zap.NewDevelopmentConfig()
	}

	if level == "" {
		return nil, nil, errors.New("missing log level")
	}

	var l zapcore.Level
	if err := l.Set(level); err != nil {
		return nil, nil, err
	}

	loggerConfig.Level.SetLevel(l)

	logger, err := loggerConfig.Build()
	if err != nil {
		return nil, nil, err
	}
	return logger, zap.NewStdLog(logger), nil
}
