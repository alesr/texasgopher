package cards

import (
	"encoding/json"
	"net/http"

	"gitlab.com/alesr/texasgopher/internal/rest"
	"go.uber.org/zap"
)

var _ Resource = (*DefaultResource)(nil)

// Resource defines the rest resource interface.
type Resource interface {
	PostEvaluate(w http.ResponseWriter, req *http.Request)
}

// DefaultResource defines a default resource data structure.
type DefaultResource struct {
	logger  *zap.Logger
	service Service
}

type evaluationRequest struct {
	Cards string `json:"cards"`
}

type evaluationResponse struct {
	Hand     string `json:"hand"`
	Deck     string `json:"deck"`
	BestHand string `json:"best_hand"`
}

// NewResource instantiates a new default resource.
func NewResource(logger *zap.Logger, service Service) *DefaultResource {
	return &DefaultResource{
		logger:  logger,
		service: service,
	}
}

// PostEvaluate is a HTTP POST handler for evaluating the best hand.
func (r *DefaultResource) PostEvaluate(w http.ResponseWriter, req *http.Request) {
	var data evaluationRequest

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		rest.WriteJSON(w, req, rest.NewErrorResp(http.StatusInternalServerError, ""))
		return
	}

	r.logger.Info("processing evaluation", zap.String("cards", data.Cards))

	_, err := r.service.Validate(data.Cards)
	if err != nil {
		r.logger.Info("could not validate cards", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
		rest.WriteJSON(w, req, rest.NewErrorResp(http.StatusBadRequest, "invalid cards"))
		return
	}

	evaluation, err := r.service.Evaluate(data.Cards)
	if err != nil {
		r.logger.Info("could not evalute cards", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
		rest.WriteJSON(w, req, rest.NewErrorResp(http.StatusBadRequest, "could not evaluate cards"))
		return
	}
	rest.WriteJSON(w, req, evaluationResponse{
		Hand:     evaluation.Hand,
		Deck:     evaluation.Deck,
		BestHand: evaluation.BestHand,
	})
}
