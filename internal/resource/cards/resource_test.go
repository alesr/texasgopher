package cards

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"go.uber.org/zap"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name         string
		givenLogger  *zap.Logger
		givenService Service
		expected     *DefaultResource
	}{
		{
			name:         "empty",
			givenLogger:  nil,
			givenService: nil,
			expected:     &DefaultResource{nil, nil},
		},
		{
			name:         "complete",
			givenLogger:  zap.NewNop(),
			givenService: &MockService{logger: zap.NewNop()},
			expected: &DefaultResource{
				logger:  zap.NewNop(),
				service: &MockService{logger: zap.NewNop()},
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := NewResource(tc.givenLogger, tc.givenService)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestPostEvaluate(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name                   string
		given                  evaluationRequest
		givenServiceEvaluation Evaluation
		expected               evaluationResponse
	}{
		{
			name: "valid",
			given: evaluationRequest{
				Cards: "AC 2D 9C 3S KD 5S 4D KS AS 4C",
			},
			givenServiceEvaluation: Evaluation{
				Hand:     "AC 2D 9C 3S KD",
				Deck:     "5S 4D KS AS 4C",
				BestHand: "dummy-hand",
			},
			expected: evaluationResponse{
				Hand:     "AC 2D 9C 3S KD",
				Deck:     "5S 4D KS AS 4C",
				BestHand: "dummy-hand",
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {

			mockService := MockService{
				logger: zap.NewNop(),
				FuncEvaluate: func(string) (Evaluation, error) {
					return tc.givenServiceEvaluation, nil
				},
			}

			resource := DefaultResource{
				logger:  zap.NewNop(),
				service: &mockService,
			}

			rr := httptest.NewRecorder()

			body, err := json.Marshal(tc.given)
			require.NoError(t, err)

			u, err := url.Parse("/")
			require.NoError(t, err)

			req, err := http.NewRequest(http.MethodPost, u.String(), bytes.NewReader(body))
			require.NoError(t, err)

			resource.PostEvaluate(rr, req)

			var resp evaluationResponse
			dec := json.NewDecoder(rr.Body)
			require.NoError(t, dec.Decode(&resp))

			assert.Equal(t, tc.expected, resp)
		})
	}
}
