package cards

import (
	"fmt"
	"strings"

	"gitlab.com/alesr/texasgopher/internal/resource/cards/texasgopher"
	"go.uber.org/zap"
)

var _ Service = (*DefaultService)(nil)

const allowedNumCards = 10

// Service defines the interface type.
type Service interface {
	Validate(cards string) (bool, error)
	Evaluate(cards string) (Evaluation, error)
}

// DefaultService implements the Service interface.
type DefaultService struct {
	logger *zap.Logger
}

// NewService instantiates a new psygopher default service.
func NewService(logger *zap.Logger) *DefaultService {
	return &DefaultService{
		logger: logger,
	}
}

// Validate checks if the input cards are valid.
func (s *DefaultService) Validate(cardsReq string) (bool, error) {
	cards := s.parseCards(cardsReq)

	if !s.validateCardsLen(cards) {
		return false, fmt.Errorf("could not validate cards: %+v", cards)
	}

	game, err := texasgopher.NewGame(cards)
	if err != nil {
		return false, err
	}

	if err := game.Validate(); err != nil {
		return false, err
	}
	return true, nil
}

func (s *DefaultService) parseCards(rawCards string) []string {
	cards := strings.ToUpper(rawCards)
	return strings.Split(cards, " ")
}

func (s *DefaultService) validateCardsLen(cards []string) bool {
	for _, c := range cards {
		if len(c) != 2 {
			return false
		}
	}

	return len(cards) == allowedNumCards
}

// Evaluation defines the result of an evaluation.
type Evaluation struct {
	Hand     string
	Deck     string
	BestHand string
}

// Evaluate evaluates the best hand considering the cards in hand and in the deck.
func (s *DefaultService) Evaluate(cardsReq string) (Evaluation, error) {
	cards := s.parseCards(cardsReq)

	if !s.validateCardsLen(cards) {
		return Evaluation{}, fmt.Errorf("could not validate cards: %+v", cards)
	}

	game, err := texasgopher.NewGame(cards)
	if err != nil {
		return Evaluation{}, err
	}

	hand, deck, bestHand := game.Evaluate()
	return Evaluation{
		Hand:     hand,
		Deck:     deck,
		BestHand: bestHand,
	}, nil
}
