package cards

import (
	"go.uber.org/zap"
)

var _ Service = (*MockService)(nil)

// MockService implements the Service interface.
type MockService struct {
	logger       *zap.Logger
	FuncValidate func(string) (bool, error)
	FuncEvaluate func(string) (Evaluation, error)
}

// Validate implements the game interface.
func (m *MockService) Validate(cardsReq string) (bool, error) {
	if m.FuncValidate != nil {
		b, err := m.FuncValidate(cardsReq)
		if err != nil {
			return false, err
		}
		return b, nil
	}
	return false, nil
}

// Evaluate implements the game interface.
func (m *MockService) Evaluate(cardsReq string) (Evaluation, error) {
	if m.FuncEvaluate != nil {
		eval, err := m.FuncEvaluate(cardsReq)
		if err != nil {
			return Evaluation{}, err
		}
		return eval, nil
	}
	return Evaluation{}, nil
}
