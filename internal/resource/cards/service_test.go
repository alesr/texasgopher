package cards

import (
	"testing"

	"github.com/stretchr/testify/require"

	"go.uber.org/zap"

	"github.com/stretchr/testify/assert"
)

func TestAllowedNumCards(t *testing.T) {
	expected := 10
	assert.Equal(t, expected, allowedNumCards)
}

func TestNewDefaultServer(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    *zap.Logger
		expected *DefaultService
	}{
		{
			name:     "success",
			given:    zap.NewNop(),
			expected: &DefaultService{logger: zap.NewNop()},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := NewService(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestValidateCards(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		service  *DefaultService
		given    string
		expected bool
	}{
		{
			name:     "success",
			given:    "AC 2D 9C 3S KD 5S 4D KS AS 4C",
			expected: true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual, err := tc.service.Validate(tc.given)
			require.NoError(t, err)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestValidateCardsErrors(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		service  *DefaultService
		given    string
		expected bool
	}{
		{
			name:     "empty",
			given:    "",
			expected: false,
		},
		{
			name:     "notEnoughCards",
			given:    "AC 2D 9C 3S",
			expected: false,
		},
		{
			name:     "tooManyCards",
			given:    "AC 2D 9C 3S KD 5S 4D KS AS 4C AD 2C",
			expected: false,
		},
		{
			name:     "invalidCard",
			given:    "AC 2D 9C 3S KD 5S 4D KS AS XX",
			expected: false,
		},
		{
			name:     "dupeCard",
			given:    "AC 2D 9C 3S KD 5S 4D KS 4C 4C",
			expected: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual, err := tc.service.Validate(tc.given)
			require.Error(t, err)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestParse(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		service  *DefaultService
		given    string
		expected []string
	}{
		{
			name:     "empty",
			service:  NewService(nil),
			given:    "",
			expected: []string{""},
		},
		{
			name:    "success",
			service: NewService(nil),
			given:   "aa AB bA BB bC CB Cc",
			expected: []string{
				"AA", "AB", "BA", "BB", "BC", "CB", "CC",
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := tc.service.parseCards(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestValidateCardsLen(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		service  *DefaultService
		given    []string
		expected bool
	}{
		{
			name:     "empty",
			service:  NewService(nil),
			given:    []string{""},
			expected: false,
		},
		{
			name:    "success",
			service: NewService(nil),
			given: []string{
				"AC", "2D", "9C", "3S", "KD",
				"5S", "4D", "KS", "AS", "4C",
			},
			expected: true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := tc.service.validateCardsLen(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}
