package texasgopher

import (
	"sort"
)

// straightFlush checks for five cards with sequential value.
func straightFlush(cards []card) bool {
	vals, ok := sameFace(cards)
	if !ok {
		return false
	}
	return straight(vals)
}

// fourOfAKind checks for four cards with same value.
func fourOfAKind(cards []card) bool { return valueCounter(cards, 4) }

// fullHouse checks for three cards with same value and two card of another same value.
// TODO: can be optimized.
func fullHouse(cards []card) bool {
	type counter struct {
		repeat int
		card
	}

	s := make(map[string]counter)
	for _, c := range cards {
		if _, ok := s[c.value]; !ok {
			s[c.value] = counter{repeat: 1, card: c}
			continue
		}
		s[c.value] = counter{
			repeat: s[c.value].repeat + 1,
			card:   s[c.value].card,
		}
	}

	r := make([]int, 0) // repeated higher than 2 and 3
	var seen string
	for _, v := range s {
		if v.repeat >= 2 {
			seen = v.value
			r = append(r, v.repeat)
			delete(s, v.value)
			continue
		}
		if v.repeat >= 3 && v.value != seen {
			r = append(r, v.repeat)
		}
	}

	if len(r) >= 2 {
		for _, v := range r {
			if v >= 3 {
				return true
			}
		}
	}
	return false
}

// flush checks for five cards of same face.
func flush(cards []card) bool { return faceCounter(cards, 5) }

// straight checks for five cards with sequential values.
func straight(cards []card) bool {
	baseRank := valuesBaseRank(cards)

	// Ranking cards.
	s := rankValues(cards, baseRank)

	// Sort cards in ranking order and check if there's a 5 len sequence.

	sort.Sort(s)

	unique := removeDupeRankedValues(s)

	lastRank := unique[0].rank
	counter := 1

	for i := 1; i <= len(unique)-1; i++ {
		v := unique[i]

		// Rank value is consecutive.
		if v.rank-lastRank == 1 {
			// Update counter: if we see 5 consecutive we have a straight
			counter++
			if counter >= 5 {
				return true
			}
		} else {
			counter = 0
		}
		lastRank = v.rank
	}
	return false
}

// threeOfaKind checks for three cards of same value.
func threeOfaKind(cards []card) bool { return valueCounter(cards, 3) }

// twoPairs checks for two pairs of card of same value.
func twoPairs(cards []card) bool {
	var pairs int
	m := make(map[string]int)

	for _, c := range cards {
		if _, ok := m[c.value]; !ok {
			m[c.value] = 1
			continue
		}

		m[c.value]++
		if m[c.value] == 2 { // pair found
			pairs++

			if pairs == 2 {
				return true
			}

			// We don't want to keep looking for more pair of same value.
			delete(m, c.value)
		}
	}
	return false
}

// onaPair checks for a pair of cards of same value.
func onePair(cards []card) bool { return valueCounter(cards, 2) }

// highestCard (aka: "simple nothing") checks for the highest card.
func highestCard() bool { return true }
