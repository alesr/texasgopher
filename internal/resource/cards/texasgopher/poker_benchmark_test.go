package texasgopher

import "testing"

func BenchmarkHighCard(b *testing.B) {
	for n := 0; n < b.N; n++ {
		highestCard()
	}
}

func BenchmarkOnePair(b *testing.B) {
	input := []card{
		{"6", "C"}, {"9", "C"}, {"8", "C"}, {"2", "D"}, {"7", "C"},
		{"2", "H"}, {"T", "C"}, {"4", "C"}, {"9", "S"}, {"A", "H"},
	}
	for n := 0; n < b.N; n++ {
		onePair(input)
	}
}

func BenchmarkTwoPairs(b *testing.B) {
	input := []card{
		{"A", "H"}, {"2", "C"}, {"9", "S"}, {"A", "D"}, {"3", "C"},
		{"Q", "H"}, {"K", "S"}, {"J", "S"}, {"J", "D"}, {"K", "D"},
	}
	for n := 0; n < b.N; n++ {
		twoPairs(input)
	}
}

func BenchmarkThreeOfaKind(b *testing.B) {
	input := []card{
		{"K", "S"}, {"A", "H"}, {"2", "H"}, {"3", "C"}, {"4", "H"},
		{"K", "C"}, {"2", "C"}, {"T", "C"}, {"2", "D"}, {"A", "S"},
	}
	for n := 0; n < b.N; n++ {
		threeOfaKind(input)
	}
}

func BenchmarkStraight(b *testing.B) {
	input := []card{
		{"A", "C"}, {"2", "D"}, {"9", "C"}, {"3", "S"}, {"K", "D"},
		{"K", "C"}, {"T", "D"}, {"K", "S"}, {"Q", "S"}, {"J", "C"},
	}
	for n := 0; n < b.N; n++ {
		straight(input)
	}
}

func BenchmarkFlush(b *testing.B) {
	input := []card{
		{"2", "H"}, {"A", "D"}, {"5", "H"}, {"A", "C"}, {"7", "H"},
		{"A", "H"}, {"6", "H"}, {"9", "H"}, {"4", "H"}, {"3", "C"},
	}
	for n := 0; n < b.N; n++ {
		flush(input)
	}
}

func BenchmarkFullHouse(b *testing.B) {
	input := []card{
		{"2", "H"}, {"2", "S"}, {"3", "H"}, {"3", "S"}, {"3", "C"},
		{"2", "D"}, {"3", "D"}, {"6", "C"}, {"9", "C"}, {"T", "H"},
	}
	for n := 0; n < b.N; n++ {
		fullHouse(input)
	}
}

func BenchmarkFourOfAKind(b *testing.B) {
	input := []card{
		{"T", "H"}, {"J", "H"}, {"Q", "C"}, {"Q", "D"}, {"Q", "S"},
		{"Q", "H"}, {"K", "H"}, {"A", "H"}, {"2", "S"}, {"6", "S"},
	}
	for n := 0; n < b.N; n++ {
		fourOfAKind(input)
	}
}

func BenchmarkStraightFlush(b *testing.B) {
	input := []card{
		{"T", "H"}, {"J", "H"}, {"Q", "C"}, {"Q", "D"}, {"Q", "S"},
		{"Q", "H"}, {"K", "H"}, {"A", "H"}, {"2", "S"}, {"6", "S"},
	}
	for n := 0; n < b.N; n++ {
		straightFlush(input)
	}
}
