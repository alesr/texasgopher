package texasgopher

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSameFace(t *testing.T) {
	t.Skip("not yet implemented")
}
func TestValueCounter(t *testing.T) {
	t.Skip("not yet implemented")
}

func TestMapValues(t *testing.T) {
	t.Skip("not yet implemented")
}

func TestValuesBaseRank(t *testing.T) {
	t.Skip("not yet implemented")
}

func TestLowRank(t *testing.T) {
	expected := map[string]int{
		"A": 0,
		"2": 1,
		"3": 2,
		"4": 3,
		"5": 4,
		"6": 5,
		"7": 6,
		"8": 7,
		"9": 8,
		"T": 9,
		"J": 10,
		"Q": 11,
		"K": 12,
	}

	assert.Equal(t, expected, lowRank)
}

func TestHighRank(t *testing.T) {
	expected := map[string]int{
		"2": 0,
		"3": 1,
		"4": 2,
		"5": 3,
		"6": 4,
		"7": 5,
		"8": 6,
		"9": 7,
		"T": 8,
		"J": 9,
		"Q": 10,
		"K": 11,
		"A": 12,
	}

	assert.Equal(t, expected, highRank)
}

func TestRankValues(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name       string
		givenCards []card
		givenRank  map[string]int
		expected   rankedCards
	}{
		{
			name: "lowRank",
			givenCards: []card{
				{"A", "S"},
				{"2", "D"},
				{"6", "C"},
				{"Q", "S"},
				{"K", "C"},
			},
			givenRank: lowRank,
			expected: rankedCards{
				{rank: 0, card: card{"A", "S"}},
				{rank: 1, card: card{"2", "D"}},
				{rank: 5, card: card{"6", "C"}},
				{rank: 11, card: card{"Q", "S"}},
				{rank: 12, card: card{"K", "C"}},
			},
		},
		{
			name: "highRank",
			givenCards: []card{
				{"A", "S"},
				{"T", "D"},
				{"J", "C"},
				{"Q", "S"},
				{"K", "C"},
			},
			givenRank: highRank,
			expected: rankedCards{
				{rank: 12, card: card{"A", "S"}},
				{rank: 8, card: card{"T", "D"}},
				{rank: 9, card: card{"J", "C"}},
				{rank: 10, card: card{"Q", "S"}},
				{rank: 11, card: card{"K", "C"}},
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := rankValues(tc.givenCards, tc.givenRank)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestRankValuesPanic(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name       string
		givenCards []card
		givenRank  map[string]int
	}{
		{
			name: "unknownCard",
			givenCards: []card{
				{"A", "S"},
				{"2", "X"},
				{"6", "C"},
				{"Q", "S"},
				{"1", "C"},
			},
			givenRank: lowRank,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			panicFn := func() { _ = rankValues(tc.givenCards, tc.givenRank) }
			assert.Panics(t, panicFn)
		})
	}
}

func TestRemoveDupeRankedValues(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    rankedCards
		expected rankedCards
	}{
		{
			name: "dupe",
			given: rankedCards{
				rankedCard{
					rank: 0,
					card: card{"A", "C"},
				}, rankedCard{
					rank: 0,
					card: card{"3", "S"},
				}, rankedCard{
					rank: 0,
					card: card{"A", "D"},
				},
			},
			expected: rankedCards{
				rankedCard{
					rank: 0,
					card: card{"A", "C"},
				},
				rankedCard{
					rank: 0,
					card: card{"3", "S"},
				},
			},
		},
		{
			name: "noDupe",
			given: rankedCards{
				rankedCard{
					rank: 0,
					card: card{"A", "C"},
				}, rankedCard{
					rank: 0,
					card: card{"3", "S"},
				}, rankedCard{
					rank: 0,
					card: card{"T", "D"},
				},
			},
			expected: rankedCards{
				rankedCard{
					rank: 0,
					card: card{"A", "C"},
				},
				rankedCard{
					rank: 0,
					card: card{"3", "S"},
				},
				rankedCard{
					rank: 0,
					card: card{"T", "D"},
				},
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actualUnique := removeDupeRankedValues(tc.given)
			assert.Equal(t, tc.expected, actualUnique)
		})
	}
}
