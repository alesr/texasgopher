package texasgopher

// sameFace returns true if at least five of same face.
func sameFace(cards []card) ([]card, bool) {
	filter := make(map[string]int)
	var preCheck bool
	var face string

	for _, c := range cards {
		if _, ok := filter[c.face]; !ok {
			filter[c.face] = 1
			continue
		}
		filter[c.face]++
		if filter[c.face] == 5 {
			preCheck = true
			face = c.face
			break
		}
	}

	result := make([]card, 0)
	for _, c := range cards {
		if c.face == face {
			result = append(result, c)
		}
	}
	return result, preCheck
}

func valueCounter(cards []card, n int) bool {
	m := make(map[string]int)

	for _, c := range cards {
		if _, ok := m[c.value]; !ok {
			m[c.value] = 1
			continue
		}

		m[c.value]++
		if m[c.value] == n {
			return true
		}
	}
	return false
}

func faceCounter(cards []card, n int) bool {
	m := make(map[string]int)

	for _, c := range cards {
		if _, ok := m[c.face]; !ok {
			m[c.face] = 1
			continue
		}

		m[c.face]++
		if m[c.face] == n {
			return true
		}
	}
	return false
}

func mapValues(cards []card) map[string]int {
	m := make(map[string]int)
	for _, c := range cards {
		if _, ok := m[c.value]; !ok {
			m[c.value] = 1
			continue
		}
		m[c.value]++
	}
	return m
}

// lowRank represents the card's ranking starting for ace and ending on king.
var lowRank = map[string]int{
	"A": 0,
	"2": 1,
	"3": 2,
	"4": 3,
	"5": 4,
	"6": 5,
	"7": 6,
	"8": 7,
	"9": 8,
	"T": 9,
	"J": 10,
	"Q": 11,
	"K": 12,
}

// highRank represents the card's ranking starting for two and ending on ace.
var highRank = map[string]int{
	"2": 0,
	"3": 1,
	"4": 2,
	"5": 3,
	"6": 4,
	"7": 5,
	"8": 6,
	"9": 7,
	"T": 8,
	"J": 9,
	"Q": 10,
	"K": 11,
	"A": 12,
}

// hankValues returns a hash table ranking the card's values.
// Aces can be used as low as high rank. eg.: AS, 2C, 3D ... or JH, QD, KS, AC.
func valuesBaseRank(cards []card) map[string]int {
	m := mapValues(cards)

	has := func(val string) bool {
		_, ok := m[val]
		return ok
	}

	// We only ace as high rank if there's a straight or full straight.
	if has("A") && has("K") && has("Q") && has("J") && has("T") {
		return highRank
	}
	return lowRank
}

func rankValues(cards []card, baseRank map[string]int) rankedCards {
	ranking := make(rankedCards, 0, len(cards))
	for _, c := range cards {
		v, ok := baseRank[c.value]
		if !ok {
			panic("failed to rank value: " + c.value)
		}
		ranking = append(ranking, rankedCard{card: c, rank: v})
	}
	return ranking
}

func removeDupeRankedValues(cards rankedCards) rankedCards {
	unique := make(rankedCards, 0)
	seen := make(map[string]struct{}, len(cards))

	for _, c := range cards {
		if _, ok := seen[c.value]; !ok {
			unique = append(unique, c)
			seen[c.value] = struct{}{}
			continue
		}
	}
	return unique
}
