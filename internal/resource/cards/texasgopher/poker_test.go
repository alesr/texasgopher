package texasgopher

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStraightFlush(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected bool
	}{
		{
			name: "sampleCase",
			given: []card{
				{"T", "H"}, {"J", "H"}, {"Q", "C"}, {"Q", "D"}, {"Q", "S"},
				{"Q", "H"}, {"K", "H"}, {"A", "H"}, {"2", "S"}, {"6", "S"},
			},
			expected: true,
		},
		{
			name: "notFull",
			given: []card{
				{"T", "H"}, {"J", "H"}, {"Q", "C"}, {"Q", "D"}, {"Q", "S"},
				{"Q", "H"}, {"K", "D"}, {"A", "H"}, {"2", "S"}, {"6", "S"},
			},
			expected: false,
		},
		{
			name: "notFound",
			given: []card{
				{"4", "C"}, {"9", "C"}, {"9", "D"}, {"2", "D"}, {"7", "C"},
				{"9", "H"}, {"T", "C"}, {"6", "C"}, {"5", "S"}, {"A", "H"},
			},
			expected: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := straightFlush(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestFourOfAKind(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected bool
	}{
		{
			name: "sampleCase",
			given: []card{
				{"2", "H"}, {"2", "S"}, {"3", "H"}, {"3", "S"}, {"3", "C"},
				{"2", "D"}, {"3", "D"}, {"6", "C"}, {"9", "C"}, {"T", "H"},
			},
			expected: true,
		},
		{
			name: "notFound",
			given: []card{
				{"A", "C"}, {"2", "D"}, {"3", "C"}, {"4", "D"}, {"5", "H"},
				{"6", "H"}, {"7", "S"}, {"8", "S"}, {"9", "S"}, {"J", "D"},
			},
			expected: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := threeOfaKind(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestFullHouse(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected bool
	}{
		{
			name: "sampleCase",
			given: []card{
				{"2", "H"}, {"2", "S"}, {"3", "H"}, {"3", "S"}, {"3", "C"},
				{"2", "D"}, {"3", "D"}, {"6", "C"}, {"9", "C"}, {"T", "H"},
			},
			expected: true,
		},
		{
			name: "notFound",
			given: []card{
				{"4", "C"}, {"9", "C"}, {"8", "D"}, {"2", "D"}, {"7", "C"},
				{"9", "H"}, {"T", "C"}, {"6", "C"}, {"5", "S"}, {"A", "H"},
			},
			expected: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := fullHouse(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestFlush(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected bool
	}{
		{
			name: "sampleCase",
			given: []card{
				{"2", "H"}, {"A", "D"}, {"5", "H"}, {"A", "C"}, {"7", "H"},
				{"A", "H"}, {"6", "H"}, {"9", "H"}, {"4", "H"}, {"3", "C"},
			},
			expected: true,
		},
		{
			name: "notFound",
			given: []card{
				{"A", "C"}, {"2", "D"}, {"3", "C"}, {"4", "D"}, {"5", "H"},
				{"6", "H"}, {"7", "S"}, {"8", "S"}, {"9", "S"}, {"A", "H"},
			},
			expected: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := flush(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestStraight(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name         string
		given        []card
		expectedBool bool
	}{
		{
			name: "sampleCase",
			given: []card{
				{"A", "C"}, {"2", "D"}, {"9", "C"}, {"3", "S"}, {"K", "D"},
				{"5", "S"}, {"4", "D"}, {"K", "S"}, {"A", "S"}, {"4", "C"},
			},
			expectedBool: true,
		},
		{
			name: "alternative",
			given: []card{
				{"A", "C"}, {"2", "D"}, {"9", "C"}, {"3", "S"}, {"K", "D"},
				{"K", "C"}, {"T", "D"}, {"K", "S"}, {"Q", "S"}, {"J", "C"},
			},
			expectedBool: true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actualBool := straight(tc.given)
			assert.Equal(t, tc.expectedBool, actualBool)
		})
	}
}

func TestThreeOfaKind(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected bool
	}{
		{
			name: "sampleCase",
			given: []card{
				{"K", "S"}, {"A", "H"}, {"2", "H"}, {"3", "C"}, {"4", "H"},
				{"K", "C"}, {"2", "C"}, {"T", "C"}, {"2", "D"}, {"A", "S"},
			},
			expected: true,
		},
		{
			name: "notFound",
			given: []card{
				{"A", "C"}, {"2", "D"}, {"3", "C"}, {"4", "D"}, {"5", "H"},
				{"6", "H"}, {"7", "S"}, {"8", "S"}, {"5", "D"}, {"2", "H"},
			},
			expected: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := threeOfaKind(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestTwoPairs(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected bool
	}{
		{
			name: "sampleCase",
			given: []card{
				{"A", "H"}, {"2", "C"}, {"9", "S"}, {"A", "D"}, {"3", "C"},
				{"Q", "H"}, {"K", "S"}, {"J", "S"}, {"J", "D"}, {"K", "D"},
			},
			expected: true,
		},
		{
			name: "notFound",
			given: []card{
				{"A", "C"}, {"2", "C"}, {"3", "C"}, {"4", "D"}, {"5", "C"},
				{"6", "H"}, {"7", "C"}, {"8", "C"}, {"9", "S"}, {"T", "H"},
			},
			expected: false,
		},
		{
			name: "notFoundSamePair",
			given: []card{
				{"A", "C"}, {"2", "C"}, {"3", "C"}, {"A", "D"}, {"5", "C"},
				{"6", "H"}, {"7", "C"}, {"A", "C"}, {"9", "S"}, {"T", "H"},
			},
			expected: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := twoPairs(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestOnePair(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected bool
	}{
		{
			name: "sampleCase",
			given: []card{
				{"6", "C"}, {"9", "C"}, {"8", "C"}, {"2", "D"}, {"7", "C"},
				{"2", "H"}, {"T", "C"}, {"4", "C"}, {"9", "S"}, {"A", "H"},
			},
			expected: true,
		},
		{
			name: "notFound",
			given: []card{
				{"A", "C"}, {"2", "C"}, {"3", "C"}, {"4", "D"}, {"5", "C"},
				{"6", "H"}, {"7", "C"}, {"8", "C"}, {"9", "S"}, {"T", "H"},
			},
			expected: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := onePair(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestHighCard(t *testing.T) {
	expected := true
	assert.Equal(t, expected, highestCard())
}
