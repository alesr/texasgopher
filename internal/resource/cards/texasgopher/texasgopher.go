package texasgopher

import (
	"fmt"
	"strings"

	"errors"
)

const initialCardsNum = 10 // 5 hand + 5 deck

type card struct {
	value string
	face  string
}

type cards []card

func (cs cards) string() string {
	var s string
	for i, v := range cs {
		if i != 0 {
			s += " "
		}
		s += v.string()
	}
	return s
}

func (c *card) string() string { return c.value + c.face }

// rankedCard defines the data struct for ranked cards.
type rankedCard struct {
	card
	rank int
}

// rankedCards define a list of ranked cards and implements the sort.Interface.
type rankedCards []rankedCard

// Len is the number of elements in the collection.
func (rs rankedCards) Len() int {
	return len(rs)
}

// Swap swaps the elements with indexes i and j.
func (rs rankedCards) Swap(i, j int) {
	rs[i], rs[j] = rs[j], rs[i]
}

// Less reports whether the element with index i
// should sort before the element with index j.
func (rs rankedCards) Less(i, j int) bool {
	return rs[i].rank < rs[j].rank
}

// Game defines the game interface.
type Game interface {
	Validate() error
	Evaluate() (string, string, string)
	String() string
}

// SimpleGame defines the a game domain model.
type SimpleGame struct {
	hand cards
	deck cards
}

// NewGame instantiantes a new simple game.
func NewGame(cards []string) (*SimpleGame, error) {
	if len(cards) != initialCardsNum {
		return nil, errors.New("invalid number of cards")
	}

	sanitize(cards)

	hand := make([]card, 0)
	for _, c := range cards[:5] {
		hand = append(hand, card{value: string(c[0]), face: string(c[1])})
	}

	deck := make([]card, 0)
	for _, c := range cards[5:] {
		deck = append(deck, card{value: string(c[0]), face: string(c[1])})
	}
	return &SimpleGame{hand, deck}, nil
}

func sanitize(cards []string) {
	for i, c := range cards {
		cards[i] = strings.ToUpper(c)
	}
}

// Validate validates cards in hand and deck.
func (g *SimpleGame) Validate() error {
	// Validate cards

	if err := validCards(g.hand); err != nil {
		return fmt.Errorf("invalid card in hand: %s", err)
	}

	if err := validCards(g.deck); err != nil {
		return fmt.Errorf("invalid card in deck: %s", err)
	}

	if hasDuplicate(g.hand) {
		return errors.New("invalid duplicated card in hand")
	}

	if hasDuplicate(g.deck) {
		return errors.New("invalid duplicated card in deck")
	}
	return nil
}

// String returns the string representation of the cards in hand and in the deck.
func (g *SimpleGame) String() string {
	var deck, hand string
	for i, v := range g.deck {
		if i != 0 {
			deck += ", "
		}
		deck += v.value + v.face
	}
	for i, v := range g.hand {
		if i != 0 {
			hand += ", "
		}
		hand += v.value + v.face
	}
	return fmt.Sprintf("\n\tDeck: %s\n\tHand: %s", deck, hand)
}

// Evaluate evaluates the hand and deck cards and return the best poker combination.
func (g *SimpleGame) Evaluate() (string, string, string) {
	cards := append(g.hand, g.deck...)

	if straightFlush(cards) {
		return g.hand.string(), g.deck.string(), "straight-flush"
	}

	if fourOfAKind(cards) {
		return g.hand.string(), g.deck.string(), "four-of-a-kind"
	}

	if fullHouse(cards) {
		return g.hand.string(), g.deck.string(), "full-house"
	}

	if flush(cards) {
		return g.hand.string(), g.deck.string(), "flush"
	}

	if straight(cards) {
		return g.hand.string(), g.deck.string(), "straight"
	}

	if threeOfaKind(cards) {
		return g.hand.string(), g.deck.string(), "three-of-a-kind"
	}

	if twoPairs(cards) {
		return g.hand.string(), g.deck.string(), "two-pairs"
	}

	if onePair(cards) {
		return g.hand.string(), g.deck.string(), "one-pair"
	}

	if highestCard() {
		return g.hand.string(), g.deck.string(), "highest-card"
	}
	return "", "", ""
}
