package texasgopher

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestInitialCardsNum(t *testing.T) {
	expected := 10
	assert.Equal(t, expected, initialCardsNum)
}

func TestNewGame(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []string
		expected *SimpleGame
	}{
		{
			name:  "valid",
			given: []string{"3D", "5S", "2H", "QD", "TD", "6S", "KH", "9H", "AD", "QH"},
			expected: &SimpleGame{
				hand: []card{
					{"3", "D"},
					{"5", "S"},
					{"2", "H"},
					{"Q", "D"},
					{"T", "D"},
				},
				deck: []card{
					{"6", "S"},
					{"K", "H"},
					{"9", "H"},
					{"A", "D"},
					{"Q", "H"},
				},
			},
		},
		{
			name:  "withLowercase",
			given: []string{"3D", "5S", "2H", "qD", "TD", "6s", "KH", "9H", "Ad", "QH"},
			expected: &SimpleGame{
				hand: []card{
					{"3", "D"},
					{"5", "S"},
					{"2", "H"},
					{"Q", "D"},
					{"T", "D"},
				},
				deck: []card{
					{"6", "S"},
					{"K", "H"},
					{"9", "H"},
					{"A", "D"},
					{"Q", "H"},
				},
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual, err := NewGame(tc.given)
			require.NoError(t, err)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestNewGameErrors(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []string
		expected error
	}{
		{
			name:     "empty",
			given:    nil,
			expected: errors.New("invalid number of cards"),
		},
		{
			name:     "invalidCardsNum",
			given:    []string{"3D", "5S", "2H", "QD", "TD", "6S", "KH", "9H", "AD"},
			expected: errors.New("invalid number of cards"),
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			_, err := NewGame(tc.given)
			require.Error(t, err)
			assert.EqualError(t, err, tc.expected.Error())
		})
	}
}

func TestValidate(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name  string
		given *SimpleGame
	}{
		{
			name: "valid",
			given: &SimpleGame{
				hand: []card{
					{"A", "C"},
					{"2", "D"},
					{"9", "C"},
					{"3", "S"},
					{"K", "D"},
				},
				deck: []card{
					{"5", "S"},
					{"4", "D"},
					{"K", "S"},
					{"A", "S"},
					{"4", "C"},
				},
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.given.Validate()
			require.NoError(t, err)
		})
	}
}

func TestValidateErrors(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name  string
		given *SimpleGame
	}{
		{
			name: "invalidCardInHand",
			given: &SimpleGame{
				hand: []card{
					{"A", "C"},
					{"2", "D"},
					{"9", "C"},
					{"3", "S"},
					{"*", "*"},
				},
				deck: []card{
					{"5", "S"},
					{"4", "D"},
					{"K", "S"},
					{"A", "S"},
					{"4", "C"},
				},
			},
		},
		{
			name: "invalidCardInDeck",
			given: &SimpleGame{
				hand: []card{
					{"A", "C"},
					{"2", "D"},
					{"9", "C"},
					{"3", "S"},
					{"K", "D"},
				},
				deck: []card{
					{"5", "S"},
					{"4", "D"},
					{"K", "S"},
					{"A", "S"},
					{"*", "*"},
				},
			},
		},
		{
			name: "invalidCardDuplicateInHand",
			given: &SimpleGame{
				hand: []card{
					{"A", "C"},
					{"2", "D"},
					{"9", "C"},
					{"K", "D"},
					{"K", "D"},
				},
				deck: []card{
					{"5", "S"},
					{"4", "D"},
					{"K", "S"},
					{"A", "S"},
					{"4", "C"},
				},
			},
		},
		{
			name: "invalidCardDuplicateInDeck",
			given: &SimpleGame{
				hand: []card{
					{"A", "C"},
					{"2", "D"},
					{"9", "C"},
					{"3", "S"},
					{"K", "D"},
				},
				deck: []card{
					{"5", "S"},
					{"4", "D"},
					{"K", "S"},
					{"4", "C"},
					{"4", "C"},
				},
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.given.Validate()
			require.Error(t, err)
		})
	}
}

func TestString(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    *SimpleGame
		expected string
	}{
		{
			name: "complete",
			given: &SimpleGame{
				hand: []card{
					{"A", "C"},
					{"2", "D"},
					{"9", "C"},
					{"3", "S"},
					{"K", "D"},
				},
				deck: []card{
					{"5", "S"},
					{"4", "D"},
					{"K", "S"},
					{"A", "S"},
					{"4", "C"},
				},
			},
			expected: "\n\tDeck: 5S, 4D, KS, AS, 4C\n\tHand: AC, 2D, 9C, 3S, KD",
		},
		{
			name: "incomplete",
			given: &SimpleGame{
				hand: []card{},
				deck: []card{
					{"5", "S"},
					{"4", "D"},
					{"K", "S"},
					{"A", "S"},
					{"4", "C"},
				},
			},
			expected: "\n\tDeck: 5S, 4D, KS, AS, 4C\n\tHand: ",
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := tc.given.String()
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestEvaluate(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name             string
		given            *SimpleGame
		expectedHand     string
		expectedDeck     string
		expectedBestHand string
	}{
		{
			name: "straightFlushSampleCase",
			given: &SimpleGame{
				hand: []card{{"T", "H"}, {"J", "H"}, {"Q", "C"}, {"Q", "H"}, {"Q", "S"}},
				deck: []card{{"2", "C"}, {"K", "H"}, {"A", "H"}, {"2", "S"}, {"6", "S"}},
			},
			expectedHand:     "TH JH QC QH QS",
			expectedDeck:     "2C KH AH 2S 6S",
			expectedBestHand: "straight-flush",
		},
		{
			name: "fourOfAKindSampleCase",
			given: &SimpleGame{
				hand: []card{{"2", "H"}, {"2", "S"}, {"3", "H"}, {"3", "S"}, {"3", "C"}},
				deck: []card{{"2", "D"}, {"3", "D"}, {"6", "C"}, {"9", "C"}, {"T", "H"}},
			},
			expectedHand:     "2H 2S 3H 3S 3C",
			expectedDeck:     "2D 3D 6C 9C TH",
			expectedBestHand: "four-of-a-kind",
		},
		{
			name: "fullHouseSampleCaseButActuallyFourOfaKind", // Documentation provides four of a kind instead of full house.
			given: &SimpleGame{
				hand: []card{{"2", "H"}, {"2", "S"}, {"3", "H"}, {"3", "S"}, {"3", "C"}},
				deck: []card{{"2", "D"}, {"9", "C"}, {"3", "D"}, {"6", "C"}, {"T", "H"}},
			},
			expectedHand:     "2H 2S 3H 3S 3C",
			expectedDeck:     "2D 9C 3D 6C TH",
			expectedBestHand: "four-of-a-kind",
		},
		{
			name: "fullHouseSampleCaseAlternative",
			given: &SimpleGame{
				hand: []card{{"2", "H"}, {"2", "S"}, {"3", "H"}, {"3", "S"}, {"8", "C"}},
				deck: []card{{"2", "D"}, {"9", "C"}, {"3", "D"}, {"6", "C"}, {"T", "H"}},
			},
			expectedHand:     "2H 2S 3H 3S 8C",
			expectedDeck:     "2D 9C 3D 6C TH",
			expectedBestHand: "full-house",
		},
		{
			name: "flushSampleCase",
			given: &SimpleGame{
				hand: []card{{"2", "H"}, {"A", "D"}, {"5", "H"}, {"A", "C"}, {"7", "H"}},
				deck: []card{{"A", "H"}, {"6", "H"}, {"9", "H"}, {"4", "H"}, {"3", "C"}},
			},
			expectedHand:     "2H AD 5H AC 7H",
			expectedDeck:     "AH 6H 9H 4H 3C",
			expectedBestHand: "flush",
		},
		{
			name: "straightSampleCase",
			given: &SimpleGame{
				hand: []card{{"A", "C"}, {"2", "D"}, {"9", "C"}, {"3", "S"}, {"K", "D"}},
				deck: []card{{"5", "S"}, {"4", "D"}, {"K", "S"}, {"A", "S"}, {"4", "C"}},
			},
			expectedHand:     "AC 2D 9C 3S KD",
			expectedDeck:     "5S 4D KS AS 4C",
			expectedBestHand: "straight",
		},
		{
			name: "threeOfaKindSampleCaseButActuallyFullHouse", // Documentation provides full house instead of tree of a kind.
			given: &SimpleGame{
				hand: []card{{"K", "S"}, {"A", "H"}, {"2", "H"}, {"3", "C"}, {"4", "H"}},
				deck: []card{{"K", "C"}, {"2", "C"}, {"T", "C"}, {"2", "D"}, {"A", "S"}},
			},
			expectedHand:     "KS AH 2H 3C 4H",
			expectedDeck:     "KC 2C TC 2D AS",
			expectedBestHand: "full-house",
		},
		{
			name: "threeOfaKindSampleCaseAlternative",
			given: &SimpleGame{
				hand: []card{{"K", "S"}, {"A", "H"}, {"2", "H"}, {"K", "H"}, {"4", "H"}},
				deck: []card{{"K", "C"}, {"3", "C"}, {"T", "C"}, {"7", "D"}, {"Q", "S"}},
			},
			expectedHand:     "KS AH 2H KH 4H",
			expectedDeck:     "KC 3C TC 7D QS",
			expectedBestHand: "three-of-a-kind",
		},
		{
			name: "twoPairsSampleCase",
			given: &SimpleGame{
				hand: []card{{"A", "H"}, {"2", "C"}, {"9", "S"}, {"A", "D"}, {"3", "C"}},
				deck: []card{{"Q", "H"}, {"K", "S"}, {"J", "S"}, {"J", "D"}, {"K", "D"}},
			},
			expectedHand:     "AH 2C 9S AD 3C",
			expectedDeck:     "QH KS JS JD KD",
			expectedBestHand: "two-pairs",
		},
		{
			name: "onePairSampleCaseButActuallyFlush", // Documentation provides flush instead of one pair.
			given: &SimpleGame{
				hand: []card{{"6", "C"}, {"9", "C"}, {"8", "C"}, {"2", "D"}, {"7", "C"}},
				deck: []card{{"2", "H"}, {"T", "C"}, {"4", "C"}, {"9", "S"}, {"A", "H"}},
			},
			expectedHand:     "6C 9C 8C 2D 7C",
			expectedDeck:     "2H TC 4C 9S AH",
			expectedBestHand: "flush",
		},
		{
			name: "onePairSampleCaseAlternative",
			given: &SimpleGame{
				hand: []card{{"2", "C"}, {"9", "S"}, {"8", "D"}, {"Q", "D"}, {"7", "C"}},
				deck: []card{{"2", "H"}, {"T", "C"}, {"4", "C"}, {"3", "S"}, {"A", "H"}},
			},
			expectedHand:     "2C 9S 8D QD 7C",
			expectedDeck:     "2H TC 4C 3S AH",
			expectedBestHand: "one-pair",
		},
		{
			name: "highestCardSampleCaseButActuallyOnePair", // Documentation provides one pair instead of high card.
			given: &SimpleGame{
				hand: []card{{"3", "D"}, {"5", "S"}, {"2", "H"}, {"Q", "D"}, {"T", "D"}},
				deck: []card{{"6", "S"}, {"K", "H"}, {"9", "H"}, {"A", "D"}, {"Q", "H"}},
			},
			expectedHand:     "3D 5S 2H QD TD",
			expectedDeck:     "6S KH 9H AD QH",
			expectedBestHand: "one-pair",
		},
		{
			name: "highestCardSampleCaseAlternative",
			given: &SimpleGame{
				hand: []card{{"3", "D"}, {"5", "S"}, {"2", "H"}, {"Q", "D"}, {"T", "D"}},
				deck: []card{{"6", "S"}, {"K", "H"}, {"9", "H"}, {"A", "D"}, {"7", "C"}},
			},
			expectedHand:     "3D 5S 2H QD TD",
			expectedDeck:     "6S KH 9H AD 7C",
			expectedBestHand: "highest-card",
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actualHand, actualDeck, actualBestHand := tc.given.Evaluate()
			assert.Equal(t, tc.expectedHand, actualHand)
			assert.Equal(t, tc.expectedDeck, actualDeck)
			assert.Equal(t, tc.expectedBestHand, actualBestHand)
		})
	}
}
