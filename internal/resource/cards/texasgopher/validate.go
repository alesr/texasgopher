package texasgopher

import (
	"errors"
	"fmt"
)

var cardCollection = []string{
	"2H", "3H", "4H", "5H", "6H", "7H", "8H", "9H", "TH", "JH", "QH", "KH", "AH",
	"2D", "3D", "4D", "5D", "6D", "7D", "8D", "9D", "TD", "JD", "QD", "KD", "AD",
	"2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "TC", "JC", "QC", "KC", "AC",
	"2S", "3S", "4S", "5S", "6S", "7S", "8S", "9S", "TS", "JS", "QS", "KS", "AS",
}

func validCards(cards []card) error {
	if len(cards) == 0 {
		return errors.New("no cards for validation")
	}

	for _, card := range cards {
		if !contains(cardCollection, card) {
			return fmt.Errorf("could not validate card: '%s'", card.string())
		}
	}
	return nil
}

func contains(list []string, value card) bool {
	for _, l := range list {
		if l == value.string() {
			return true
		}
	}
	return false
}

func hasDuplicate(cards []card) bool {
	m := make(map[string]int)

	for _, card := range cards {
		if _, ok := m[card.string()]; !ok {
			m[card.string()] = 1
			continue
		}
		m[card.string()]++
	}

	for _, v := range m {
		if v > 1 {
			return true
		}
	}
	return false
}
