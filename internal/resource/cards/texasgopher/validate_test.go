package texasgopher

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCardCollection(t *testing.T) {
	expected := []string{
		"2H", "3H", "4H", "5H", "6H", "7H", "8H", "9H", "TH", "JH", "QH", "KH", "AH",
		"2D", "3D", "4D", "5D", "6D", "7D", "8D", "9D", "TD", "JD", "QD", "KD", "AD",
		"2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "TC", "JC", "QC", "KC", "AC",
		"2S", "3S", "4S", "5S", "6S", "7S", "8S", "9S", "TS", "JS", "QS", "KS", "AS",
	}
	assert.Equal(t, expected, cardCollection)
}

func TestValidCards(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name  string
		given []card
	}{
		{
			name: "valid1",
			given: []card{
				{value: "A", face: "H"},
			},
		},
		{
			name: "valid2",
			given: []card{
				{value: "A", face: "C"},
			},
		},
		{
			name: "valid3",
			given: []card{
				{value: "5", face: "S"},
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			err := validCards(tc.given)
			require.NoError(t, err)
		})
	}
}

func TestValidCardsErrors(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected error
	}{
		{
			name:     "nilValue",
			given:    nil,
			expected: errors.New("no cards for validation"),
		},
		{
			name:     "empty",
			given:    []card{},
			expected: errors.New("no cards for validation"),
		},
		{
			name: "invalidSymbol",
			given: []card{
				{"X", "X"},
			},
			expected: errors.New("could not validate card: 'XX'"),
		},
		{
			name: "invalidLen",
			given: []card{
				{"", "ASD"},
			},
			expected: errors.New("could not validate card: 'ASD'"),
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			err := validCards(tc.given)
			assert.EqualError(t, err, tc.expected.Error())
		})
	}
}

func TestContains(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name      string
		givenList []string
		givenCard card
		expected  bool
	}{
		{
			name:      "nillList",
			givenList: nil,
			givenCard: card{},
			expected:  false,
		},
		{
			name:      "contain",
			givenList: []string{"7C"},
			givenCard: card{"7", "C"},
			expected:  true,
		},
		{
			name:      "dontContain",
			givenList: []string{"6C", "AS"},
			givenCard: card{"7", "S"},
			expected:  false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := contains(tc.givenList, tc.givenCard)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestHasDuplicate(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    []card
		expected bool
	}{
		{
			name:     "empty",
			given:    nil,
			expected: false,
		},
		{
			name: "notDuplicate",
			given: []card{
				{"A", "S"},
				{"2", "H"},
			},
			expected: false,
		},
		{
			name: "duplicate",
			given: []card{
				{"3", "S"},
				{"3", "S"},
			},
			expected: true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := hasDuplicate(tc.given)
			assert.Equal(t, tc.expected, actual)
		})
	}
}
