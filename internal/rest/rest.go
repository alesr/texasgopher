package rest

// There are better ways to handle rest errors when they come from the service layer.
// The concern of this implementation is rather to have a data type that we can use
// to send over the wire and communicate that the resource could not handle the request.

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// ErrorResp defines the rest error response data structure.
type ErrorResp struct {
	StatusCode int    `json:"status_code,omitempty"`
	StatusMsg  string `json:"status_message,omitempty"`
	Reason     string `json:"reason,omitempty"`
}

func (e *ErrorResp) Error() string {
	if e.StatusCode == 0 {
		return ""
	}
	return fmt.Sprintf("%s - %s: %s", strconv.Itoa(e.StatusCode), e.StatusMsg, e.Reason)
}

// NewErrorResp returns a rest error response.
func NewErrorResp(code int, reason string) *ErrorResp {
	msg := http.StatusText(code)
	if msg == "" {
		log.Println(fmt.Errorf("unsupported status code: %d", code))
		return &ErrorResp{http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), ""}
	}
	return &ErrorResp{code, msg, reason}
}

// WriteJSON encodes the received data and send it as a JSON payload.
func WriteJSON(w http.ResponseWriter, req *http.Request, v interface{}) {
	w.Header().Set("Content-Type", "application/json")
	if status, ok := req.Context().Value("Status").(int); ok {
		w.WriteHeader(status)
	}

	var buf bytes.Buffer

	if v != nil {
		enc := json.NewEncoder(&buf)
		enc.SetEscapeHTML(true)
		if err := enc.Encode(v); err != nil {
			log.Println("could not decode response data:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	if _, err := w.Write(buf.Bytes()); err != nil {
		log.Println("could not write response:", err)
	}
}
