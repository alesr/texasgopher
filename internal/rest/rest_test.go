package rest

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"
)

func TestError(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    *ErrorResp
		expected string
	}{
		{
			name:     "empty",
			given:    &ErrorResp{},
			expected: "",
		},
		{
			name: "complete",
			given: &ErrorResp{
				StatusCode: http.StatusUnauthorized,
				StatusMsg:  http.StatusText(http.StatusUnauthorized),
				Reason:     "foo",
			},
			expected: "401 - Unauthorized: foo",
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := tc.given.Error()
			assert.Equal(t, tc.expected, actual)
		})
	}
}
func TestNewErrorResp(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name            string
		givenStatusCode int
		givenReason     string
		expected        *ErrorResp
	}{
		{
			name:            "valid",
			givenStatusCode: http.StatusBadRequest,
			givenReason:     "foo",
			expected: &ErrorResp{
				StatusCode: http.StatusBadRequest,
				StatusMsg:  http.StatusText(http.StatusBadRequest),
				Reason:     "foo",
			},
		},
		{
			name:            "noReason",
			givenStatusCode: http.StatusBadRequest,
			givenReason:     "",
			expected: &ErrorResp{
				StatusCode: http.StatusBadRequest,
				StatusMsg:  http.StatusText(http.StatusBadRequest),
				Reason:     "",
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := NewErrorResp(tc.givenStatusCode, tc.givenReason)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestNewErrorRespErrors(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name            string
		givenStatusCode int
		givenReason     string
		expected        *ErrorResp
	}{
		{
			name:            "invalidStatusCode",
			givenStatusCode: 0,
			givenReason:     "foo",
			expected: &ErrorResp{
				StatusCode: http.StatusInternalServerError,
				StatusMsg:  http.StatusText(http.StatusInternalServerError),
				Reason:     "",
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := NewErrorResp(tc.givenStatusCode, tc.givenReason)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestWriteJSON(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		given    interface{}
		expected string
	}{
		{
			name:     "empty",
			given:    struct{}{},
			expected: "{}\n",
		},
		{
			name:     "empty2",
			given:    nil,
			expected: "",
		},
		{
			name: "data1",
			given: struct {
				Value string `json:"value"`
			}{
				Value: "bar",
			},
			expected: "{\"value\":\"bar\"}\n",
		},
		{
			name: "data2",
			given: struct {
				Value string `json:"foo"`
			}{
				Value: "quz",
			},
			expected: "{\"foo\":\"quz\"}\n",
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			rr := httptest.NewRecorder()

			req, err := http.NewRequest("", "", nil)
			require.NoError(t, err)

			WriteJSON(rr, req, tc.given)

			assert.Equal(t, tc.expected, rr.Body.String())
		})
	}
}
