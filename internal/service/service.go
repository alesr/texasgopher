package service

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/alesr/texasgopher/internal/config"
	"gitlab.com/alesr/texasgopher/internal/resource/cards"
	"gitlab.com/alesr/texasgopher/internal/rest"
	"go.uber.org/zap"
)

// Service defines a service data structure.
type Service struct {
	addr      string
	logger    *zap.Logger
	stdLogger *log.Logger
	server    *http.Server
}

// Start starts the service.
func (s *Service) Start() error {
	s.logger.Info("running service...", zap.String("addr", s.addr))
	return s.server.ListenAndServe()
}

// Stop stops the service.
func (s *Service) Stop(ctx context.Context, cancel context.CancelFunc) error {
	s.logger.Info("stopping service...", zap.String("addr", s.addr))

	defer cancel()
	s.server.SetKeepAlivesEnabled(false)
	return s.server.Shutdown(ctx)
}

// New instantiates a new service.
func New(ctx context.Context, conf *config.Config) (*Service, error) {
	cardsService := cards.NewService(conf.Logger)
	cardsResource := cards.NewResource(conf.Logger, cardsService)

	r := chi.NewRouter()

	r.Get("/", rootHandler)

	r.Group(func(r chi.Router) {
		r.Use(middleware.StripSlashes)

		r.Post("/cards/evaluate", cardsResource.PostEvaluate)
	})

	server := NewHTTPServer(
		ctx,
		conf.StdLogger,
		conf.DefaultServerReadTimeout,
		conf.DefaultServerWriteTimeout,
		conf.DefaultServerIdleTimeout,
		r,
		conf.Addr,
	)

	return &Service{
		addr:      conf.Addr,
		logger:    conf.Logger,
		stdLogger: conf.StdLogger,
		server:    server,
	}, nil
}

func rootHandler(w http.ResponseWriter, req *http.Request) {
	rest.WriteJSON(w, req, struct{}{})
}

// NewHTTPServer instantiates a new HTTP server.
func NewHTTPServer(
	ctx context.Context,
	stdLogger *log.Logger,
	readTimeout time.Duration,
	writeTimeout time.Duration,
	idleTimeout time.Duration,
	handler http.Handler,
	addr string,
) *http.Server {
	return &http.Server{
		Addr:         addr,
		Handler:      chi.ServerBaseContext(ctx, handler),
		ErrorLog:     stdLogger,
		ReadTimeout:  readTimeout,
		WriteTimeout: writeTimeout,
		IdleTimeout:  idleTimeout,
	}
}
