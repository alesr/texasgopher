package integration

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type IntegrationT struct {
	*testing.T
	client             *http.Client
	texasGopherBaseURL url.URL
}

func TestIntegration(t *testing.T) {
	texasGopherBaseURL := os.Getenv("TEXASGOPHERBASEURL")
	if texasGopherBaseURL == "" {
		panic("missing texas gopher server base url")
	}

	u, err := url.Parse(texasGopherBaseURL)
	require.NoError(t, err)

	it := IntegrationT{t, http.DefaultClient, *u}

	it.RunIT("texasGopherServer", testTexasGopherServer)
}

func (it IntegrationT) RunIT(name string, testFunc func(it IntegrationT)) bool {
	return it.Run("texasGopherServer", func(t *testing.T) {
		testFunc(IntegrationT{t, it.client, it.texasGopherBaseURL})
	})
}

type evaluationRequest struct {
	Cards string `json:"cards"`
}

type evaluationResponse struct {
	Hand     string `json:"hand"`
	Deck     string `json:"deck"`
	BestHand string `json:"best_hand"`
}

func testTexasGopherServer(it IntegrationT) {
	it.Parallel()

	cases := []struct {
		name     string
		given    evaluationRequest
		expected evaluationResponse
	}{
		{
			name: "evaluate",
			given: evaluationRequest{
				Cards: "AC 2D 9C 3S KD 5S 4D KS AS 4C",
			},
			expected: evaluationResponse{
				Hand:     "AC 2D 9C 3S KD",
				Deck:     "5S 4D KS AS 4C",
				BestHand: "straight",
			},
		},
	}

	for _, tc := range cases {
		it.Run(tc.name, func(t *testing.T) {
			u := it.texasGopherBaseURL
			u.Path = path.Join(u.Path, "evaluate")

			body, err := json.Marshal(tc.given)
			require.NoError(it, err)

			req, err := http.NewRequest(http.MethodPost, u.String(), bytes.NewReader(body))
			require.NoError(it, err)

			resp, err := it.client.Do(req)
			require.NoError(it, err)

			defer func() {
				if err := resp.Body.Close(); err != nil {
					require.NoError(t, err)
				}
			}()

			var evalResp evaluationResponse
			dec := json.NewDecoder(resp.Body)
			require.NoError(it, dec.Decode(&evalResp))

			assert.Equal(it, tc.expected, evalResp)
		})
	}
}
